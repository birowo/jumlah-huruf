package main

import (
	"os"
	"sort"
	"strings"
)

func UpperCase(chr rune) rune {
	if chr >= 'a' && chr <= 'z' {
		chr = chr - 'a' + 'A'
	}
	return chr
}
func JumlahTiapHuruf(buffer []byte) (jumlah map[rune]int, total int) {
	jumlah = map[rune]int{}
	for _, chr := range string(buffer) {
		if chr < ' ' { //skip \r, \n, \t, ...
			continue
		}
		jumlah[chr]++
		total++
	}
	return
}

type JumlahHuruf struct {
	Huruf  rune
	Jumlah int
}

func UrutHuruf(jumlah map[rune]int) (urut []JumlahHuruf) {
	urut = make([]JumlahHuruf, 0, len(jumlah))
	for k, v := range jumlah {
		urut = append(urut, JumlahHuruf{k, v})
	}
	sort.Slice(urut, func(i, j int) bool {
		return UpperCase(urut[i].Huruf) < UpperCase(urut[j].Huruf)
	})
	return
}
func CetakJumlahVocal(jumlah map[rune]int) {
	sep := [2]string{", ", ": "}
	println("Jumlah vocal:")
	vocals := [5]string{"Aa", "Ee", "Ii", "Oo", "Uu"}
	for _, vocal := range vocals {
		jmlh := 0
		for i, chr := range vocal {
			jmlh += jumlah[chr]
			print(string(chr), sep[i])
		}
		println(jmlh)
	}
}
func main() {
	var buffer [999]byte
	for {
		println("\nSilahkan ketik sembarang lalu [Enter]:")
		n, _ := os.Stdin.Read(buffer[:])
		if n < 2 || (buffer[0] == '\r' && buffer[1] == '\n') {
			return
		}
		print("Tampilkan inputan:\n", string(buffer[:]))
		jumlah, total := JumlahTiapHuruf(buffer[:])
		println("Total huruf:", total)
		CetakJumlahVocal(jumlah)
		println("Urut berdasar abjad:")
		urut := UrutHuruf(jumlah)
		n = 0
		for _, item := range urut {
			n += copy(buffer[n:], strings.Repeat(string(item.Huruf), item.Jumlah))
		}
		println(string(buffer[:n]))
	}
}
